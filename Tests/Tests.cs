﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using GlueSharp;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        private const string URL = "ws://127.0.0.1:5000";

        private const int PollInterval = 0;
        private const int TimeOutInterval = 500;

        public string TestName
        {
            get { return TestContext.CurrentContext.Test.Name; }
        }

        private Task<bool> EvaluateOverTime(Func<bool> f, bool shouldSucceed)
        {
            var t = Task.Run(async () =>
            {
                var sw = new Stopwatch();
                sw.Start();
                while (sw.ElapsedMilliseconds < TimeOutInterval)
                {
                    if (f()) return shouldSucceed;
                    await Task.Delay(PollInterval);
                }

                Console.WriteLine("Timed Out");
                return !shouldSucceed;
            });
            return t;
        }

        private Task<bool> Eventually(Func<bool> f)
        {
            return EvaluateOverTime(f, true);
        }

        private Task<bool> Never(Func<bool> f)
        {
            return EvaluateOverTime(f, false);
        }

        private Task Wait(long interval)
        {
            var t = Task.Run(() =>
            {
                var sw = new Stopwatch();
                sw.Start();
                while (sw.ElapsedMilliseconds < interval)
                {
                    Thread.Sleep(1);
                }
            });
            return t;
        }

        #region Domains

        [Test]
        public async Task OnWelcomeIsCalledWhenConnected()
        {
            var c = new Client(URL, TestName);
            var welcomeCalled = false;

            c.OnWelcome = domain => { welcomeCalled = true; };

            Assert.That(await Eventually(() =>
            {
                c.Update();
                return welcomeCalled;
            }));
        }

        [Test]
        public async Task ReceivesDomainNameOnWelcome()
        {
            var c = new Client(URL, TestName);
            var success = false;

            c.OnWelcome = domain => { success = domain == TestName; };

            Assert.That(await Eventually(() =>
            {
                c.Update();
                return success;
            }));
        }

        [Test]
        public async Task CanCreateMultipleConnections()
        {
            var c2 = new Client(URL, TestName);
            var c1 = new Client(URL, TestName);

            var firstConnected = false;
            var secondConnected = false;

            c1.OnWelcome = domain => { firstConnected = true; };

            c2.OnWelcome = domain => { secondConnected = true; };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return firstConnected && secondConnected;
            }));
        }

        [Test]
        public async Task ClientsInTheSameDomainCanCommunicate()
        {
            var saidHello = false;

            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var listener = new Role("Listener");
            listener.SetMessageHandler("hello", source => { saidHello = true; });
            c1.SetRole(listener);

            var greeter = new Role("Greeter");
            c2.SetRole(greeter);

            greeter.OnWelcome = domain => { greeter.Subscribe("Listener", l => { greeter.SendTo(l, "hello"); }); };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return saidHello;
            }));
        }

        [Test]
        public async Task PreventsDifferentDomainsFromCommunicating()
        {
            var domain1 = TestName + "1";
            var domain2 = TestName + "2";
            var saidHello = false;

            var c1 = new Client(new Client.Options(URL, domain1));
            var c2 = new Client(new Client.Options(URL, domain2));

            var listener = new Role("Listener");
            listener.SetMessageHandler("hello", source => { saidHello = true; });

            var greeter = new Role("Greeter");

            c1.SetRole(listener);
            c2.SetRole(greeter);

            greeter.OnWelcome = domain => { greeter.Subscribe("Listener", l => { greeter.SendTo(l, "hello"); }); };

            Assert.That(await Never(() =>
            {
                c1.Update();
                c2.Update();
                return saidHello;
            }));
        }

        [Test]
        public async Task CanTransferDomains()
        {
            var domain1 = TestName + "1";
            var domain2 = TestName + "2";
            var c = new Client(new Client.Options(URL, domain1));

            var transferred = false;
            c.OnWelcome = domain =>
            {
                c.TransferDomain(domain2);
                transferred = c.TargetDomain == domain2;
            };

            Assert.That(await Eventually(() =>
            {
                c.Update();
                return transferred;
            }));
        }

        [Test]
        public async Task DisconnectsWhenTransferring()
        {
            var domain1 = TestName + "1";
            var domain2 = TestName + "2";
            var c = new Client(new Client.Options(URL, domain1));

            c.OnWelcome = domain =>
            {
                if (c.TargetDomain == domain1)
                {
                    c.TransferDomain(domain2);
                    Assert.IsFalse(c.IsConnected);
                }
            };

            await Eventually(() =>
            {
                c.Update();
                return c.IsConnected;
            });
        }

        [Test]
        public async Task CallsOnWelcomeWhenReconnecting()
        {
            var domain1 = TestName + "1";
            var domain2 = TestName + "2";
            var c = new Client(new Client.Options(URL, domain1));

            var timesCalled = 0;

            c.OnWelcome = domain =>
            {
                timesCalled++;
                c.TransferDomain(domain2);
            };

            Assert.That(await Eventually(() =>
            {
                c.Update();
                return timesCalled == 2;
            }));
        }

        [Test]
        public void ThrowsExceptionIfTransferDomainCalledWhenNotConnected()
        {
            var c = new Client(URL, TestName);
            try
            {
                c.TransferDomain("Not connected");
                // If we get this far there was no exception...
                Assert.Fail();
            }
            catch (Exception)
            {
                Assert.True(true);
                //There's an exception so all good
            }
        }

        #endregion

        #region Connecting

        [Test]
        public async Task ConnectsOnCreation()
        {
            var domain = TestName;
            var c = new Client(new Client.Options(URL, domain));
            Assert.That(await Eventually(() =>
            {
                c.Update();
                return c.IsConnected;
            }));
        }

        [Test]
        public async Task CallsOnCloseIfItCannotConnect()
        {
            var c = new Client(new Client.Options("ws://not.a.valid.url", TestName));
            var didClose = false;
            c.OnClose = (wasExpected, code, reason) => { didClose = true; };

            Assert.That(await Eventually(() =>
            {
                c.Update();
                return didClose;
            }));
        }

        [Test]
        public void HasIdOnCreation()
        {
            var c = new Client(URL, TestName);
            Assert.That(c.Id > 0);
        }

        [Test]
        public async Task CorrectlyReportsDisconnectedStatus()
        {
            var c = new Client(URL, TestName);

            await Eventually(() =>
            {
                c.Update();
                return c.IsConnected;
            });
            c.CloseConnection();
            Assert.That(!c.IsConnected);
        }

        [Test]
        public async Task IsConnectedAfterOnWelcome()
        {
            var c = new Client(URL, TestName);
            var success = false;
            c.OnWelcome = domain => { success = c.IsConnected; };
            Assert.That(await Eventually(() =>
            {
                c.Update();
                return success;
            }));
        }

        [Test]
        public async Task ItCanSendContextForRouter()
        {
            var c = new Client(URL, TestName, new {shouldFail = false});
            var success = false;
            c.OnWelcome = domain => { success = true; };

            Assert.That(await Eventually(() =>
            {
                c.Update();
                return success;
            }));
        }

        [Test]
        public async Task ItCanFailToConnect()
        {
            var c = new Client(URL, TestName, new {shouldFail = true});
            var failed = false;
            c.OnClose = (expected, code, reason) => { failed = true; };

            Assert.That(await Eventually(() =>
            {
                c.Update();
                return failed;
            }));
        }

        #endregion

        #region Client

        [Test]
        public async Task ClosesWhenCloseConnectionIsCalled()
        {
            var c = new Client(URL, TestName);
            var closed = false;
            c.OnClose = (expected, code, reason) => { closed = true; };
            await Eventually(() =>
            {
                c.Update();
                return c.IsConnected;
            });

            c.CloseConnection();

            Assert.That(await Eventually(() =>
            {
                c.Update();
                return closed;
            }));
        }

        [Test]
        public async Task OnCloseIsCalledAfterCloseConnectionIsCalled()
        {
            var c = new Client(URL, TestName);
            var closed = false;
            c.OnClose = (expected, code, reason) => { closed = true; };
            await Eventually(() =>
            {
                c.Update();
                return c.IsConnected;
            });

            c.CloseConnection();
            Assert.That(await Eventually(() =>
            {
                c.Update();
                return closed;
            }));
        }

        [Test]
        public async Task ClosesFromCloseConnectionAreExpected()
        {
            var c = new Client(URL, TestName);
            var expected = false;
            c.OnClose = (e, code, reason) => { expected = e; };
            c.OnWelcome = domain => { c.CloseConnection(); };
            Assert.That(await Eventually(() =>
            {
                c.Update();
                return expected;
            }));
        }

        #endregion

        #region Messages

        [Test]
        public async Task CanSendMessage()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var receiver = new Role("Receiver");
            var received = false;
            receiver.SetMessageHandler("Test", source => { received = true; });

            var sender = new Role("Sender");

            c1.SetRole(receiver);
            c2.SetRole(sender);

            sender.OnWelcome = domain => { sender.Subscribe("Receiver", r => { sender.SendTo(r, "Test"); }); };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return received;
            }));
        }

        [Test]
        public async Task DoesNotReceiveMessagesItTargetsToItself()
        {
            var tester = new Role("Tester");
            var received = false;
            tester.SetMessageHandler("Test", source => { received = true; });

            var c = new Client(URL, TestName);
            c.SetRole(tester);

            tester.OnWelcome = domain => { tester.Subscribe("Tester", t => { tester.SendTo(t, "Test"); }); };

            Assert.That(await Never(() =>
            {
                c.Update();
                return received;
            }));
        }

        [Test]
        public async Task CanSendDataWithMessage()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var receiver = new Role("Receiver");
            string data = null;
            receiver.SetMessageHandler("Test", (string d, RoleProxy source) => { data = d; });
            c1.SetRole(receiver);

            var sender = new Role("Sender");
            c2.SetRole(sender);

            sender.OnWelcome = domain => { sender.Subscribe("Receiver", r => { sender.SendTo(r, "Test", "HELLO"); }); };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return data == "HELLO";
            }));
        }

        [Test]
        public async Task ReceivesSenderWithMessage()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var a = new Role("A");
            var b = new Role("B");

            c1.SetRole(a);
            c2.SetRole(b);

            ulong senderId = 0;
            b.SetMessageHandler("Test", source => senderId = source.Id);

            a.OnWelcome = d => a.Subscribe("B", r => a.SendTo(r, "Test"));

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return senderId == a.Id;
            }));
        }

        #endregion

        #region Requests

        [Test]
        public async Task ReceivesFailedErrorIfTargetHasException()
        {
            var a = new Role("A");
            a.SetRequestHandler<string>("Test", source => { throw new Exception("BLARRGHH!!"); });

            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);
            c1.SetRole(a);

            var b = new Role("B");
            c2.SetRole(b);

            var errType = RequestError.ErrorType.None;

            b.OnWelcome = domain =>
            {
                b.Subscribe("A",
                    r =>
                    {
                        b.RequestFrom(r, "Test",
                            (string data, RequestError err, RoleProxy source) => { errType = err.Type; });
                    });
            };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return errType == RequestError.ErrorType.RequestFailed;
            }));
        }

        [Test]
        public async Task CanSendDataWithRequest()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var sender = new Role("Sender");
            c1.SetRole(sender);

            var receiver = new Role("Receiver");
            receiver.SetRequestHandler("Test", (bool data, RoleProxy source) => data);

            var success = false;
            c2.SetRole(receiver);

            sender.OnWelcome = domain =>
            {
                sender.Subscribe("Receiver",
                    r =>
                    {
                        sender.RequestFrom(r, "Test", true,
                            (bool responseData, RequestError error, RoleProxy source) => { success = responseData; });
                    });
            };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return success;
            }));
        }

        #endregion

        #region Roles

        [Test]
        public async Task CanSubscribeBeingCreated()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var a = new Role("A");
            c1.SetRole(a);

            var roleName = "NOT A ROLE NAME";

            a.OnWelcome = domain =>
            {
                a.Subscribe("B", role => { roleName = role.Role; });
                c2.SetRole(new Role("B"));
            };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return roleName == "B";
            }));
        }

        [Test]
        public async Task CanSubscribeBeingDestroyed()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var listener = new Role("Listener");
            var a = new Role("A");
            c1.SetRole(listener);
            c2.SetRole(a);

            var disconnectedRole = "";

            listener.OnWelcome = domain =>
            {
                Console.WriteLine("L WELCOME");
                listener.Subscribe("A", aRole => { }, aRole => { disconnectedRole = aRole.Role; });
            };

            a.OnWelcome = domain =>
            {
                Console.WriteLine("A WELCOME");
                a.Subscribe("Listener", listenerRole => { c2.CloseConnection(); });
            };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return disconnectedRole == "A";
            }));
        }

        [Test]
        public async Task CanStopListeningForChanges()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var a = new Role("A");
            var b = new Role("B");

            c1.SetRole(a);
            c2.SetRole(b);

            var numTimes = 0;

            a.OnWelcome = d =>
            {
                a.Subscribe(b.Name, proxy =>
                    {
                        numTimes++;
                        a.Unsubscribe(b.Name);
                    },
                    proxy => numTimes++);
            };

            Assert.That(await Never(() =>
            {
                c1.Update();
                c2.Update();
                return numTimes > 1;
            }));
        }

        [Test]
        public async Task WillListenToAllRolesAssociatedWithClient()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var a = new Role("A");
            var b = new Role("B");
            var c = new Role("C");

            c1.SetRole(a);
            c2.SetRole(b);
            c2.SetRole(c);

            var notifiedAboutB = false;
            var notifiedAboutC = false;

            a.OnWelcome = domain =>
            {
                a.Subscribe("B", role => { notifiedAboutB = role.Role == "B"; });
                a.Subscribe("C", role => { notifiedAboutC = role.Role == "C"; });
            };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return notifiedAboutB && notifiedAboutC;
            }));
        }

        [Test]
        public async Task CanGetExistingRoleInstances()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var a = new Role("A");
            var b = new Role("B");

            c1.SetRole(a);
            c2.SetRole(b);

            var id = (ulong) 0;
            a.OnWelcome = domain => { a.Subscribe("B", role => { id = role.Id; }); };
            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return id == c2.Id;
            }));
        }

        [Test]
        public async Task SendsAllExistingClientsWithRoleWhenSubscribing()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);
            var c3 = new Client(URL, TestName);

            var a = new Role("A");
            var b = new Role("B");

            c1.SetRole(a);
            c2.SetRole(b);
            c3.SetRole(b);

            var sentC2 = false;
            var sentC3 = false;

            a.OnWelcome = domain =>
            {
                a.Subscribe("B",
                    role =>
                    {
                        if (role.Id == c2.Id)
                        {
                            sentC2 = true;
                        }
                        else if (role.Id == c3.Id)
                        {
                            sentC3 = true;
                        }
                    }
                );
            };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                c3.Update();
                return sentC3 && sentC2;
            }));
        }

        [Test]
        public async Task DoesNotReceiveRoleUpdatesAboutItself()
        {
            var c = new Client(URL, TestName);

            var a = new Role("A");
            var b = new Role("B");

            c.SetRole(a);

            var failed = false;
            a.OnWelcome = domain =>
            {
                a.Subscribe("B", r => { failed = true; }, r => { failed = true; });
                c.SetRole(b);
            };
            Assert.That(await Never(() =>
            {
                c.Update();
                return failed;
            }));
        }

        [Test]
        public async Task CanSendRequestsThroughRoleInstance()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var a = new Role("A");
            var b = new Role("B");

            c1.SetRole(a);
            c2.SetRole(b);

            b.SetRequestHandler("GET_NAME", source => "Bob");

            var RequestData = "";
            a.OnWelcome = domain =>
            {
                a.Subscribe("B",
                    role =>
                    {
                        a.RequestFrom(role, "GET_NAME",
                            (string name, RequestError error, RoleProxy source) => { RequestData = name; });
                    }
                );
            };
            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return RequestData == "Bob";
            }));
        }

        [Test]
        public async Task CanSendMessagesThroughRoleInstance()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var a = new Role("A");
            var b = new Role("B");

            c1.SetRole(a);
            c2.SetRole(b);

            var received = false;
            b.SetMessageHandler("HELLO", source => { received = true; });

            a.OnWelcome = domain => { a.Subscribe("B", role => { a.SendTo(role, "HELLO"); }); };

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();
                return received;
            }));
        }

        [Test]
        public async Task CachesRoles()
        {
            var c1 = new Client(URL, TestName);
            var c2 = new Client(URL, TestName);

            var a = new Role("A");
            var b = new Role("B");

            c1.SetRole(a);
            c2.SetRole(b);

            a.OnWelcome = domain => { a.Subscribe("B"); };

            // Not sure why this is necessary to have the test work...
            await Task.Run(async () =>
            {
                while (!(c1.IsConnected && c2.IsConnected)) await Task.Delay(1);
            });

            Assert.That(await Eventually(() =>
            {
                c1.Update();
                c2.Update();

                foreach (var roleProxy in a.GetProxies("B"))
                {
                    if (roleProxy.Id == b.Id) return true;
                }

                return false;
            }));
        }

        #endregion

        #region Heartbeats

        [Test]
        public void CanChangeTheHeartbeatInterval()
        {
            var c = new Client(URL, TestName) {HeartbeatInterval = 1};
            Assert.That(c.HeartbeatInterval == 1);
        }

        [Test]
        public void CanChangeTheHeartbeatTimeout()
        {
            var c = new Client(URL, TestName) {HeartbeatTimeout = 1};
            Assert.That(c.HeartbeatTimeout == 1);
        }

        [Test]
        public async Task DoesNotSendHeartbeatsWhenClosed()
        {
            var c = new Client(URL, TestName) {HeartbeatInterval = 10};
            c.OnWelcome = domain => { c.CloseConnection(); };
            c.OnHeartbeatSent = Assert.Fail;
            await Wait((long) c.HeartbeatInterval * 2);
        }

        [Test]
        public async Task SendsHeartbeatsWhenConnected()
        {
            var c = new Client(URL, TestName) {HeartbeatInterval = 10};
            var heartbeat = false;
            c.OnHeartbeatSent = () => { heartbeat = true; };
            Assert.That(await Eventually(() =>
            {
                c.Update();
                return heartbeat;
            }));
        }

        [Test]
        public async Task EstimatedLatencyUpdatedWhenHeartbeatIsReceived()
        {
            var c = new Client(URL, TestName) {HeartbeatInterval = 5};
            var success = false;
            c.OnHeartbeatReceived = () => { success = c.EstimatedLatency >= 0; };
            Assert.That(await Eventually(() =>
            {
                c.Update();
                return success;
            }));
        }

        #endregion

        #region Reconnect

        [Test]
        public async Task AttemptsToReconnectWhenConnectionFails()
        {
            var c = new Client(URL, TestName)
            {
                Context = new {shouldNotCreateDomain = true},
                ReconnectDelay = 1
            };
            var attemptedReconnection = false;
            c.OnReconnectAttempt = () => { attemptedReconnection = true; };

            c.OnClose = (expected, code, reason) => { Console.WriteLine("CODE: " + code + ", REASON: " + reason); };

            Assert.That(await Eventually(() =>
            {
                c.Update();
                return attemptedReconnection;
            }));
        }

        [Test]
        public async Task DoesNotAttemptToReconnectWhenClosedByUser()
        {
            var c = new Client(URL, TestName);
            c.OnWelcome = domain => { c.CloseConnection(); };
            Assert.That(await Never(() =>
            {
                c.Update();
                return c.ReconnectAttempts > 0;
            }));
        }

        [Test]
        public async Task MakesLimitedReconnectionAttempts()
        {
            var c = new Client(new Client.Options("ws://localhost", TestName))
            {
                ReconnectDelay = 1,
                MaxReconnectAttempts = 3
            };

            Assert.That(await Never(() =>
            {
                c.Update();
                return c.ReconnectAttempts > c.MaxReconnectAttempts;
            }));
        }

        [Test]
        public async Task CanReconnectManually()
        {
            var c = new Client(URL, TestName) {IsAutoReconnectEnabled = false};
            var success = false;
            c.OnWelcome = domain => { c.Reconnect(); };
            c.OnReconnect = () => { success = true; };
            Assert.That(await Eventually(() =>
            {
                c.Update();
                return success;
            }));
        }

        [Test]
        public async Task CanReconnectWithDifferentConnectionContext()
        {
            var c = new Client(URL, TestName) {IsAutoReconnectEnabled = false};
            var success = false;
            c.OnWelcome = domain =>
            {
                c.Context = new {shouldFail = true};
                c.Reconnect();
                c.OnClose = (expected, code, reason) => { success = code == 4001; };
            };
            Assert.That(await Eventually(() =>
            {
                c.Update();
                return success;
            }));
        }

        #endregion
    }
}